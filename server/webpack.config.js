// Imports
const path = require("path")
require("babel-register")

// Webpack Configuration
const config = {

    // Entry
    entry: "src/index.js",

    // Output
    output: {
        path: path.resolve(__dirname, "build"),
        filename: "bundle.js"
    },

    // Loaders
    module: {
        rules : [
            {
                test: /\.js$/,
                exclude: [/node_modules/],
                use: "babel-loader"
            },
        ]
    },

    // Plugins
    plugins: []
}

// Exports
module.exports = {
    mode: "development"
}
