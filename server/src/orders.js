const reportError = require("./helpers.js").reportError
//const log = require("./helpers.js").log

class Orders {
    constructor() {
        const self = this
        this.db = {}
        /* {
            orderId: {
                id: this.order
                order:id,
                window:id,
                tab: id,
                socket: socket.id,
                user: {
                    name:current user login,
                    session: document.cookie
                }
            }
        } */
        this.get = {
            by: {
                property: function (propertyName, propertyValue) {
                    if (propertyName === "userName") {
                        return self.get.list().filter(
                            record => (
                                record.user.name === propertyValue
                            )
                        )
                    }
                    return self.get.list().filter(
                        record => record[propertyName] === propertyValue
                    )
                },
            },
            list: function () {
                return Object.keys(self.db).map(order => order = self.db[order])
            }
        }
        this.add = function (data) {
            if (!data) {
                reportError(
                    "Order add", data
                )
                return false
            }
            self.db[data.order] = data
            self.db[data.order].id = data.order
            return true
        }
        this.remove = {
            by: {
                order: function (order) {
                    return delete self.db[order]
                },
                window: function (window) {
                    const order = self.get.by
                        .property("window", window)
                    return delete self.remove.by.order(order)
                },
                userName: function (userName) {
                    const order = self.get.by
                        .property("userName", userName)
                    return delete self.remove.by.order(order)
                },
                tab: function (tab) {
                    const removed = self.get.by.property("tab", tab)
                        .filter(order => order.tab === tab)
                    removed.map(order => self.remove.by.order(order.id))
                    return removed
                },
                socket: function (socketId) {
                    const toRemove = self.get.by.property("socket", socketId)
                        .filter(order => order.socket === socketId)
                    toRemove.forEach(order => self.remove.by.order(order.id))
                    return toRemove
                }
            }
        }
    }
}
module.exports = {
    "Orders": Orders
}
