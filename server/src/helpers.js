function reportError(text, object) {
    const d = new Date().toString()
    console.error("***********\n", text, ":\n", d, "\n", object)
}
function log(text, object) {
    if (object === undefined) {
        object = ""
    }

    const date = new Date()
    const t = date.toTimeString()
    const d = date.toString()
    console.log(
        t + "\n" + "At: " + d +":\n" + text + "\n", object
    )
}
class InputChecker {
    constructor() {
        this.check = {
            user: function (user) {
                if (!user || !user.name || !user.session) {
                    return false
                } return true
            },
            session: function (session) {
                if (!session) return false
                return true
            },
            chatMessage: function (message) {
                if (!message || !message.message || !message.user) return false
                return true
            }
        }
    }
}

module.exports = {
    "InputChecker": InputChecker,
    "reportError": reportError,
    "log": log
}
