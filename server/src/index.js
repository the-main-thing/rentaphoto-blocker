const app = require("express")()
const server = app.listen(3000)
const io = require("socket.io").listen(server)
const cors = require("cors")

const log = require("./helpers.js").log
const IC = require("./helpers.js").InputChecker
const Orders = require("./orders.js").Orders
const Users = require("./models.js").Users
const Chat = require("./models.js").Chat

const orders = new Orders()
const users = new Users()
const ic = new IC()
const chat = new Chat()

app.use(cors())
app.get("/", (req, res) => {
    res.sendStatus(200)
})

io.on("connection", (socket) => {
    socket.emit("hello")
    socket.on(
        "getOrdersList",
        () => socket.emit("ordersList", orders.get.list())
    )
    // if autorization
    socket.on(
        "setUserName",
        (user) => {
            if (!ic.check.user(user)) return
            users.add(user, () => socket.emit("userSet"))
        })
    socket.on(
        "getUserName", session => {
            if (!ic.check.session(session)) return
            users.get.by.session(session).then(
                user => {
                    if (!user) socket.emit("User not found")
                    if (!user.name) socket.emit("User not found")
                    socket.emit("userName", user.name)
                }
            )
        }
    )

    // block order or tell client that it is already blocked
    socket.on("try to block", data => {
        log("try to block", data)
        if (!data) {
            socket.emit("serverError", "Invalid order")
            return
        }
        const order = orders.db[data.order]
        if (order) {
            socket.emit("order already blocked", data.window)
            return
        }
        // add connection identifier to unblock on disconnect
        data.socket = socket.id
        if (!orders.add(data)) {
            socket.emit("serverError", "Can't block order")
            return
        }
        socket.emit("order is blocked", data.window)
        io.emit("blockOrder", data)
        log("blockOrder", "order is blocked")
    })

    socket.on("try to free order", window => {
        log("try to free")
        if (!window) {
            socket.emit("serverError", "Invalid window")
            return
        }
        // we can unblock only orders with corresponding windows
        const order = orders.get.by.property("window", window)[0]
        if (!order) {
            socket.emit("serverError",
                "order already freed or never been blocked")
            return
        }
        if (!orders.remove.by.order(order.id)) {
            socket.emit("serverError", "Can't free order")
            return
        }
        log("Try to free order:", order.id)
        io.emit("freeOrder", order.id)
        log("freeOrder", "order is freed")
    })

    socket.on(
        "orderSaved", (window) => {
            if (!window) {
                socket.emit("serverError", "Invalid window")
                return
            }
            // we can unblock only orders with corresponding windows
            const order = orders.get.by.property("window", window)[0]
            if (!order) {
                socket.emit("serverError",
                    "Can't mark order changed order already freed or never been blocked")
                return
            }
            if (!orders.remove.by.order(order.id)) {
                socket.emit("serverError", "Can't free order (saveOrder)")
                return
            }
            io.emit("orderChanged", order)
            log("orderSaved", "order status is changed")
        })

    /*socket.on(
        "tabClosedByUser", (tab) => {
            log("tab closed by user", tab)
            if (!tab) return io("serverError",
                "Позвоните Паше\n Скажите, что кто-то закрыл вкладку,"
                + " но заявки не освободились.\n Некоторые заявки"
            + " могут быть заблокированны никем, в этом случае отключите"
        + " блокировщик, отредактируйте заявку и включите обратно.")
            orders.remove.by.tab(tab).map(
                order => io.emit("freeOrder", order.id)
            )
        })
    */
    socket.on(
        "disconnect", () => {
            log("socket disconnect", socket.id)
            orders.remove.by.socket(socket.id).map(
                order => io.emit("freeOrder", order.id)
            )
        })
    socket.on(
        "User logged out", name => {
            if (!name) return
            orders.get.by.property("user", name).map(
                order => {
                    if (!orders.remove.by.order(order.id)) io(
                        "serverError",
                        `Не удалось разблокировать заявку с номером ${order.id}`
                        + ` пользователя ${name}`
                        + "Позвоните, пожалуйста, Паше, сообщите об этом."
                    )
                }
            )
            io.emit("Refresh all tabs", name)
            io.emit(
                "ordersList",
                orders.get.list())
        }
    )
    socket.on(
        "Chat message from user", message => {
            chat.add(message, (error, message) => {
                if (error) return socket.emit(
                    "serverError", "can't send chat message"
                )
                io.emit(
                    "New chat message",
                    message
                )
            })
        })
    socket.on(
        "Get chat messages", () => {
            chat.get((error, messages) => {
                if (error) return socket.emit(
                    "serverError", "can't get chat message"
                )
                messages.slice(-5).map(
                    message => socket.emit("New chat message", message)
                )
            })
        }
    )
})
