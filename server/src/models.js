const mongoose = require("mongoose")

const reportError = require("./helpers.js").reportError
const log = require("./helpers.js").log

Date.prototype.addHours = function(h){
    this.setHours(this.getHours()+h)
    return this
}

setTimeout(() => {
    mongoose.connect(
        "mongodb://db/db",
        {
            server: { 
                auto_reconnect: true,
                reconnectTries: Number.MAX_VALUE,
                reconnectInterval: 1000,
                socketOptions: {keepAlive: 1, connectTimeoutMS: 30000}
            } 
        }
    )
}, 10000)


const db = mongoose.connection
db.on("error",
    console.error.bind(
        console, "connection error:"
    )
)
db.once("open", function() {
    log("Mongoose connected!")
})
const userSchema = new mongoose.Schema({
    name: {
        type:String,
        unique:true,
        required:true,
        dropDups:true
    },
    session: {
        type:String,
        unique:true,
        required:true,
        dropDups:true
    }
})
const chatSchema = new mongoose.Schema({
    text: String,
    user: String,
    date : { type : Date, default: Date.now }
})
const ChatModel = mongoose.model("Chat", chatSchema)
class Chat {
    constructor() {
        const self = this
        this.Chat = ChatModel
        this.add = function (message, callback) {
            message = new self.Chat(message)
            message.save(
                (err, message) => callback(err, message)
            )
        }
        this.get = function (callback) {
            // Date().addHours implemented in the begining of the file
            const twentyFourHoursAgo = new Date().addHours(-24)
            self.Chat.find(
                {date:
                    {$gt: twentyFourHoursAgo}
                }
            ).exec(callback)
        }
    }
}

const User = mongoose.model("Users", userSchema)
class Users {
    constructor() {
        const self = this
        this.current = null
        this.User = User
        this.add = function (user, callback) {
            self.User.findOneAndUpdate(
                {name:user.name}, user, // query for existing item, and new object
                {upsert:true}, // if there is none, create
                (err, user) => { if (err) return reportError(
                    "Error while adding user", err
                )
                return callback(user)}
            )
        }
        this.get = {
            by: {
                name: function (name, callback) {
                    self.User.findOne({name:name}).exec(
                        (error, user) => callback(error, user)
                    )
                },
                session: async function (session) {
                    const user = await self.User.findOne({session:session})
                    return user
                }
            },
            all: function (callback) {
                self.User.find({}).exec((err, users) => callback(err, users))
            }
        }
    }
}

module.exports = {
    "Users": Users,
    "Chat": Chat
}
