// Imports
const path = require("path")
const htmlWebpackPlugin = require('html-webpack-plugin')
require("babel-register")

// Webpack Configuration
const config = {

    // Entry
    entry: "src/index.js",

    // Output
    output: {
        path: path.resolve(__dirname, "build"),
        filename: "bundle.js"
    },

    // Loaders
    module: {
        rules : [
            {
                test: /\.js$/,
                exclude: [/node_modules/],
                use: "babel-loader"
            },
            // CSS Files
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.sass$/,
                use: ["style-loader", "sass-loader"]
            }
        ]
    },

    // Plugins
    plugins: [
        new htmlWebpackPlugin({
            template: "src/index.html",
            filename: "index.html",
            hash: true
        })
    ]
}

// Exports
module.exports = {
    mode: "development"
}
