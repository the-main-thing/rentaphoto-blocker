import {
    getCancelButtonOgject,
    getSaveButtonObject,
    getCloseButtonObject,
    getSideMenu,
    getAllOpenWindows,
    getRowsWithOrders
} from "./elementsGetters.js"
import {log, getSidFromWindow} from "./helpers.js"

function elementWasCreated (mutationObject) {
    return (
        mutationObject.type === "childList"
        &&
        mutationObject.addedNodes.length > 0
        &&
        mutationObject.removedNodes.length === 0
    )
}

function buttonsExists (windowObject) {
    return (
        Boolean(getSaveButtonObject(windowObject))
        &&
        Boolean(getCancelButtonOgject(windowObject))
        &&
        Boolean(getCloseButtonObject(windowObject))
    )
}
function isNodeIsTableRow (node) {
    const tableRegex = /\d{2}-\d{6}\W+(Новый|Заявка|Бронь)\W+\w+/
    if (node.innerText !== undefined) {
        if (node.innerText !== "") {
            if (node.innerText.match(tableRegex)) {
                return true
            }
        }
    }
    return false
}

function windowCreated (mutationObject, orderId) {
    // here we don't use mutationObject.addedNodes
    // because there big chance that node we need
    // already has been created
    // Instead we ask whole document for all window like
    // nodes and iterate over them, searching for node
    // that meet our requirements
    orderId = orderId.trim() // remove trailnig spaces
    if (elementWasCreated(mutationObject)) {
        console.log("windowChecker, element was created")
        const allWindows = getAllOpenWindows()
        for (let i = 0, n = allWindows.length; i < n; i++) {
            const currentWindow = allWindows[i]
            // we search only for newly created windows
            if (currentWindow.attributes.blocker) continue
            log("windowCreated checker. Window is:", currentWindow)
            const currentId = getSidFromWindow(currentWindow)
            if (currentId === orderId) return currentWindow
        }
    }
    return null
}

function tableCreated (mutationObject) {
    if (elementWasCreated(mutationObject)) {
        const nodes = mutationObject.addedNodes
        for (let i = 0, n = nodes.length; i < n; i++) {
            const node = nodes[i]
            if (isNodeIsTableRow(node)) {
                log("tableCreated")
                return true
            }
        }
    }
    return false
}
function menuIsStilOpen () {
    const sideMenu = getSideMenu()
    if (!sideMenu) return false
    if (sideMenu.style.visibility === "hidden") return false
    return true
}

function tabSwitch(mutationObject, rows) {
    if (!elementWasCreated(mutationObject)) return false
    const newAmount = document.querySelectorAll(
        "div>table[class=\"x-grid-table x-grid-table-resizer\"]>tbody>tr>td>table"
    ).length
    return newAmount !== rows
}

export { windowCreated, tableCreated, menuIsStilOpen, tabSwitch }
