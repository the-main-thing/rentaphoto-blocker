import {EventEmitter} from "events"

class CustomEvent {
    constructor() {
        this.emitter = new EventEmitter()
        this.add = function (eventName, eventHandler) {
            this.emitter.on(eventName, parameter => eventHandler(parameter))
        }

        this.emit = function (eventName, data) {
            this.emitter.emit(eventName, data)
        }
        this.on = function (eventName, eventHandler, parameter) {
            this.emitter.on(eventName, eventHandler, parameter)
        }
    }
}

export { CustomEvent }
