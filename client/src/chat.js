import {chatHtml} from "./chatHtml.js" // TODO: remove javascript from this file

class Chat {
    constructor() {
        this.created = false
        this.active = false
    }
    create(blocker) {
        // add handler for incoming messages
        blocker.socket.on("New chat message",this.recieveMessage)
        // request latest messages
        blocker.socket.emit("Get chat messages")
        // add html chat popup window
        document.querySelector("body").insertAdjacentHTML(
            "beforeend",
            chatHtml
        )

        // add listeners to elements:
        // add submit listener
        const chatForm = document.querySelector("#blocker-chat-from-element")
        chatForm.onsubmit = () => this.sendMessage(blocker)
        chatForm.onkeydown = (event) => {
            if (event.key === "Enter") this.sendMessage(blocker)
        }


        // toggle chat popup
        const chatButton = document.querySelector(".blocker-chat-open-button")
        chatButton.addEventListener("click", () => {
            chatButton.style.display = "none"
            const chatPopUp = document.querySelector("#blockerForm")
            chatPopUp.style.display = "block"
            const lastMessage = document.querySelector(
                "#blocker-messages-last-message")
            if (lastMessage) lastMessage.scrollIntoView()
        })
        const closeButton = document.querySelector(".blocker-chat-cancel")
        closeButton.addEventListener("click", () => {
            document.querySelector("#blockerForm").style.display = "none"
            chatButton.style.display = "block"
        })
        this.created = true
    }
    sendMessage(blocker) {
        const textarea = document.querySelector("#blocker-chat-textarea")
        const message = {
            user: blocker.user.name,
            text: textarea.value.toString()
        }
        if (message.text === "") return false
        blocker.socket.emit("Chat message from user", message)
        textarea.value = ""
        return false
    }
    recieveMessage(message) {
        const messagesList = document.querySelector(".blocker-chat-messages")
        // remove id from last message
        const lastMessage = document.querySelector(
            "#blocker-messages-last-message")
        if (lastMessage) lastMessage.id = ""
        const newMessage = `
            <div id="blocker-messages-last-message" class="blocker-chat-message">
            <b>${message.user}:</b>
            ${message.text}
            </div>`
        messagesList.insertAdjacentHTML(
            "beforeend", newMessage
        )
        if (lastMessage) lastMessage.scrollIntoView()
    }
}

export { Chat }
