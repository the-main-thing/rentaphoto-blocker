import {getSidFromRow} from "./helpers.js"

function getCancelButtonOgject (windowObject) {
    if (!windowObject) return null
    const button =
                    windowObject
                        .querySelector(
                            "div[class=\"x-btn x-box-item x-toolbar-item x-btn-default-small x-icon-text-left x-btn-icon-text-left x-btn-default-small-icon-text-left\"][id^=\"button-\"]>em>button>span")
                        .parentElement.parentElement.parentElement
    return button
}

function getSaveButtonObject (windowObject) {
    const button = windowObject.querySelector(".icon-save")
        .parentElement.parentElement.parentElement
    return button
}

function getCloseButtonObject (windowObject) {
    if (!windowObject) return null
    const button = windowObject.querySelector(".x-tool-close")
    return button
}

function getAllOpenWindows () {
    return Array.from(
        document.querySelectorAll(
            `div[id^="window-"][id$="-body"`
        )
    // remove all nodes with "header" in their id
    ).filter(node => !node
        .attributes.id.value.includes("header")
    )
}

// selector for rows with orders
// document.querySelectorAll('div[id^="gridpanel-"][id$="-body"][class^="x-panel-body"][class$="x-docked-noborder-left"]>div>table>tbody>tr>td>table>tbody>tr')
function getRowsWithOrders () {
    console.log("GET ROWS WITH ORDERS")
    // query all sub tables with rows
    const subTables = document
        .querySelectorAll(
            "div>table[class=\"x-grid-table x-grid-table-resizer\"]>tbody>tr"
        )
    // create array with rows
    const rows = []
    subTables.forEach(table => {
        rows.push(
            ...table.querySelectorAll("tr")
        )
    })
    // remove header rows and return 
    return rows.filter(row => {
        return (
            !row.classList.contains("header")
            &&
            !row.classList.contains("leaf")
        )
    })
}

function getRowsBySid (sid) {
    return document.querySelectorAll(`tr[blocker="${sid}"]`)
}

function getExitButton () {
    const buttons = document
        .querySelectorAll(
            "button[id^=\"button-\"][id$=\"btnEl\"][type=\"button\"][role=\"button\"][class=\"x-btn-center\"]"
        )
    for (const exitButton of buttons) {
        if (exitButton.innerText) {
            if (exitButton.innerText.includes("Выход")) {
                return exitButton
            }
        }
    }
}
function getExpandButton () {
    return document.querySelector("img[class=\"x-tool-expand-right\"]")
}
function getSideMenu () {
    return document.querySelector(
        "div[class=\"x-panel x-autowidth-table x-grid-header-hidden x-border-item x-box-item x-panel-default x-tree-panel x-tree-arrows x-grid\"]"
    )
}
function getCrmTabsBar () {
    return document.querySelector("div[id^=\"tabbar-\"][id$=\"-innerCt\"]")
        .querySelector("div[id^=\"tabbar-\"][id$=\"-targetEl\"]")
}

function getActiveCrmTab() {
    return getCrmTabsBar().querySelector(
        ".x-active.x-tab-active.x-tab-default-active.x-top-active.x-tab-top-active.x-tab-default-top-active"
    )
}
function getActiveCrmTabCloseButton() {
    return getActiveCrmTab().querySelector("a[id$=\"closeEl\"]")
}

export {
    getCancelButtonOgject,
    getSaveButtonObject,
    getCloseButtonObject,
    getRowsWithOrders,
    getRowsBySid,
    getExitButton,
    getExpandButton,
    getSideMenu,
    getCrmTabsBar,
    getAllOpenWindows,
    getActiveCrmTab,
    getActiveCrmTabCloseButton
}
