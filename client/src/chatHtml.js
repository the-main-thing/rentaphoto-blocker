const chatHtml = `
    <div class="blocker-chat-container">
    <style>
    .blocker-chat-messages {
        height: 150px;
        width: 100%;
        overflow-y: auto;
        margin-top: 1rem;
        margin-bottom: 1rem;
        padding-top: 5px;
        padding-bottom: 5px;
        padding-left: 2px;
        padding-right: 2px;
        background: rgb(250, 250, 250);
        border-top: 1px solid black;
        border-bottom: 1px solid black;
    }

    .blocker-chat-message {
    }

    /* Button used to open the chat form - fixed at the bottom of the page */
    .blocker-chat {box-sizing: border-box;}
    .blocker-chat-open-button {
      background-color: #555;
      color: white;
      padding: 16px 20px;
      border: none;
      cursor: pointer;
      opacity: 0.8;
      position: fixed;
      bottom: 23px;
      right: 28px;
      width: 280px;
    }

    /* The popup chat - hidden by default */
    .blocker-chat-popup {
      display: none;
      position: fixed;
      bottom: 0;
      right: 15px;
      border: 3px solid #f1f1f1;
      z-index: 9;
    }

    /* Add styles to the form container */
    .blocker-chat-form-container {
      max-width: 300px;
      padding: 10px;
      background-color: white;
    }

    /* Full-width textarea */
    .blocker-chat-form-container #blocker-chat-textarea {
      width: 90%;
      padding: 15px;
      margin: 5px 0 22px 0;
      border: none;
      background: #f1f1f1;
      resize: none;
      min-height: 50px;
    }

    /* When the textarea gets focus, do something */
    .blocker-chat-form-container #blocker-chat-textarea:focus {
      background-color: #ddd;
      outline: none;
    }

    /* Set a style for the submit/send button */
    .blocker-chat-form-container .blocker-chat-btn {
      background-color: #4CAF50;
      color: white;
      padding: 16px 20px;
      border: none;
      cursor: pointer;
      width: 100%;
      margin-bottom:10px;
      opacity: 0.8;
    }

    /* Add a red background color to the cancel button */
    .blocker-chat-form-container .blocker-chat-cancel {
      background-color: red;
    }

    /* Add some hover effects to buttons */
    .blocker-chat-form-container .blocker-chat-btn:hover, .blocker-chat-open-button:hover {
      opacity: 1;
    }
    </style>
    <button class="blocker-chat-open-button blocker-chat">Чат</button>

    <div class="blocker-chat-popup blocker-chat" id="blockerForm">
      <form id="blocker-chat-from-element" class="blocker-chat-form-container">
        <h3>Blocker Chat</h3>
    	<div class="blocker-chat-messages"></div>
        <label for="blockerMsg"><b>Сообщение</b></label>
        <input type="text" id="blocker-chat-textarea" placeholder="Введите сообщение" name="blcokerMsg">

        <button type="submit" class="blocker-chat-btn blocker-chat" id="blocker-submit">Отправить</button>
        <button type="button" class="blocker-chat-btn blocker-chat-cancel blocker-chat">Закрыть</button>
      </form>
    </div>

    <script>


    function closeForm() {
        document.getElementById("blockerForm").style.display = "none";
    }
    </script>
</div>
`


export {chatHtml}
