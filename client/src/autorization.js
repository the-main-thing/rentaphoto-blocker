class Autorization {
    constructor(blocker) {
        const self = this
        this.login = {
            element: document.querySelector("input[name=\"user\"]"),
            sent: false
        }

        this.observerHandler = function(mutationsList, observer) {
            for(const mutation of mutationsList) {
                if (mutation.type === "childList"
                    && mutation.addedNodes.length > 0) {
                    for (const node of mutation.addedNodes) {
                        if (node.innerText) {
                            if (node.innerText.includes("Заявки")
                                && !self.login.sent) {
                                blocker.events.emit(
                                    "setUserName", self.login.element.value
                                )
                                self.login.sent = true
                                self.addEventToButton()
                                observer.disconnect()
                            }
                        }
                    }
                }
            }
        }
        this.observer = new MutationObserver(this.observerHandler)
        this.targetNode = document.querySelector("body")
        this.config = { attributes: true, childList: true, subtree: true }

        // TODO: move logout button handling out (self.whenAutorized())
        this.init = function () {
            if (blocker.user.name) return
            console.log("spy started")
            self.login.element = document.querySelector("input[name=\"user\"]")
            if (self.login.element) {
                self.login.found = true
                self.run()
                return
            }
            blocker.events.emit("getUserName")
            self.addEventToButton()
        }
        this.run = function () {
            self.observer.observe(self.targetNode, self.config)
            console.log("autorization observer started")
            return true
        }
        this.addEventToButton = function () {
            self.getExitButton()
                .addEventListener(
                    "click",
                    () => {
                        blocker
                            .socket
                            .emit(
                                "User logged out",
                                blocker.user.name
                            )
                    })
        }
        this.getExitButton = function () {
            const buttons = document
                .querySelectorAll(
                    "button[id^=\"button-\"][id$=\"btnEl\"][type=\"button\"][role=\"button\"][class=\"x-btn-center\"]"
                )
            for (const exitButton of buttons) {
                if (exitButton.innerText) {
                    if (exitButton.innerText.includes("Выход")) {
                        return exitButton
                    }
                }
            }
        }
        this.logout = function () {
            try {
                self.getExitButton().click()
                console.log("logout yay")
            } finally {
                location.reload()
                console.log("logout reload")
            }
        }
    }
}
/*

function autorization(callback) {
    const passwd = document.querySelector("[name=\"passwd\"]")
    if (passwd) {
        let nameIsSet = false // global
        // Callback function to execute when mutations are observed
        const observerHandler = function(mutationsList, observer) {
            for(const mutation of mutationsList) {
                if (mutation.type === "childList" && mutation.addedNodes.length > 0) {
                    for (const node of mutation.addedNodes) {
                        if (node.innerText) {
                            if (node.innerText.includes("Заявки") && !nameIsSet) {
                                nameIsSet = true
                                observer.disconnect()
                                callback(passwd.value)
                            }
                        }
                    }
                }
            }
        }
        const observer = new MutationObserver(observerHandler)
        // Select the node that will be observed for mutations
        const targetNode = document.querySelector("body")
        // Options for the observer (which mutations to observe)
        const config = { attributes: true, childList: true, subtree: true }
        observer.observe(targetNode, config)
    } else {
        return "no login fiel"
    }
}
*/
export { Autorization }
