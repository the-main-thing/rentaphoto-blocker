const uuidv4 = require("uuid/v4")

import {WindowMutationObserver} from "./customObservers.js"
import {
    getCancelButtonOgject,
    getSaveButtonObject,
    getCloseButtonObject,
} from "./elementsGetters.js"

class OrderWindow {
    constructor(orderId) {
        this.id = uuidv4().toString()
        this.order = orderId
        this.alreadyBlocked = false
        this.domElementCreated = false
    }
    get domElement() {
        return document.querySelector(
            `div[blocker="${this.id}"]`
        ).parentElement
    }
    startObserver (callback) {
        this.observer = new WindowMutationObserver(
            this.order,
            windowDomObject => {
                this.domElementCreated = true
                windowDomObject.setAttribute(
                    "blocker",
                    this.id.toString()
                )
                if (callback) callback(windowDomObject)
            }
        )
        this.observer.start()
    }

    removeSaveButton () {
        getSaveButtonObject(this.domElement)
            .style.display = "none"
        console.log("REMOVESAVEBUTTON", this.domElement)
    }

    addListenersToButtons (CustomEventsObject) {
        console.log("add event listeners to window")
        // get save button
        const saveButton = getSaveButtonObject(this.domElement)
        saveButton.addEventListener("click", () => {
            CustomEventsObject.emit("orderSaved", this.id)
        })
        // get close button
        const closeButton = getCloseButtonObject(this.domElement)
        closeButton.addEventListener("click", () => {
            CustomEventsObject.emit("windowClosed", this)
        })
        // get cancel button
        const cancelButton = getCancelButtonOgject(this.domElement)
        cancelButton.addEventListener("click", () => {
            CustomEventsObject.emit("windowClosed", this)
        })
        // attach event to Esc button
        this.domElement.addEventListener("keydown", (eventObject) => {
            if (eventObject.key === "Escape") {
                CustomEventsObject.emit("windowClosed", this)
            }
        })
    }
}

export { OrderWindow }
