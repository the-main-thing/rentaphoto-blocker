import {
    getExpandButton,
    getCrmTabsBar,
    getActiveCrmTabCloseButton
} from "./elementsGetters.js"
import { 
    TabsBarObserver,
    TabSwitchObserver
} from "./customObservers"

class TabsBarHandler {
    constructor(blocker) {
        this.blocker = blocker
        this.observer = new TabsBarObserver()
    }

    get tabs() {
        return this.tabsBar.children
    }

    activate() {
        // add listener to expand button
        this.activated = true
        this.tabsBar = getCrmTabsBar()
        getExpandButton().addEventListener("click", () => {
            this.observer.start(
                this.addSwitchListener.bind(this),
                this.tabsBar
            )
        })
    }
    addSwitchListener() {
        for (const tab of this.tabs) {
            // if not already added listeners
            if (!tab.hasAttribute("blocker")) {
                tab.setAttribute("blocker", "")
                const tabObserver = new TabSwitchObserver(tab)
                tab.addEventListener("click", () => {
                    tabObserver.start(
                        () => this.blocker.events.emit("tabSwitched")
                    )
                })
            }
        }
    }
    addCloseButtonListener(orderWindowId) {
        getActiveCrmTabCloseButton().addEventListener("click", () => {
            this.blocker.events.emit("tabIsClosing", orderWindowId)
        })
    }
}

export {
    TabsBarHandler
}