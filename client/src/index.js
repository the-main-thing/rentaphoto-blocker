import { Blocker } from "./blocker.js"
import {
    getSidFromRow,
    reportError,
    log,
    rowsAmount
} from "./helpers.js"
import {toggleRowStyle} from "./painters.js"
import {
    getRowsWithOrders,
    getRowsBySid,
    getExpandButton,
} from "./elementsGetters.js"
import { TabsBarHandler } from "./crmTabsHandler"
import { Autorization } from "./autorization.js"
import { Chat } from "./chat.js"

const SERVER = "wiki.rntf.supportit.ru"

const blocker = new Blocker(SERVER)
const chat = new Chat()
const autorization = new Autorization(blocker)
const tabsHandler = new TabsBarHandler(blocker)

blocker.table.observer.start()
window.addEventListener("load", () => autorization.init())
window.addEventListener("beforeunload", () => blocker.socket.emit(
    "tabClosedByUser", blocker.tab
))

blocker.socket
    .on("hello", () => blocker.socket.emit("getOrdersList"))

blocker.socket
    .on("ordersList", orders => {
        orders.map(
            order => blocker.events.emit(
                "blockOrder", order))
        log("Orders list incoming", orders)
    })

blocker.socket
    .on("Refresh all tabs", name => {
        log("Refresh all tabs", [name, blocker.user.name])
        if (!name) return
        if (name === blocker.user.name) {
            log("Refresh all tabs", name)
            autorization.logout()
        }
    })

blocker.socket // server is telling us that is user is set
    .on("userSet", () => log("user set"))
blocker.socket // name comes from server
    .on("userName", name => blocker.user.name = name)
blocker.socket
    .on("User not found", () => autorization.logout())
blocker.socket
    .on("autorizationError",
        user => blocker.events.emit("autorizationError", user))
blocker.socket
    .on("order already blocked",
        windowId => blocker.events.emit("orderIsBlocked", windowId))
blocker.socket
    .on("blockOrder", data => blocker.events.emit("blockOrder", data))
blocker.socket
    .on("freeOrder", sid => blocker.events.emit("freeOrder", sid))
blocker.socket
    .on("orderChanged", order => blocker.events.emit("orderChanged", order))
blocker.socket
    .on("serverError",
        message => blocker.events.emit("errorFromServer", message))

blocker.events.add("getUserName", getUserName)
blocker.events.add("setUserName", setUserName)
blocker.events.add("autorizationError", autorizationError)
blocker.events.add("tableWasCreated", tableWasCreated)
blocker.events.add("openOrder", openOrder)
blocker.events.add("windowWasCreated", windowWasCreated)
blocker.events.add("orderIsBlocked", orderIsBlocked) // removes save button
blocker.events.add("addEventsToWindowButtons", addEventsToWindowButtons)
blocker.events.add("windowClosed", windowClosed)
blocker.events.add("tabIsClosing", tabIsClosing)
blocker.events.add("orderSaved", orderSaved)
blocker.events.add("blockOrder", blockOrder)
blocker.events.add("freeOrder", freeOrder)
blocker.events.add("orderChanged", orderChanged)
blocker.events.add("paintBlocked", paintBlocked)
blocker.events.add("paintChanged", paintChanged)
blocker.events.add("removeBlocks", removeBlocks)
blocker.events.add("errorFromServer", errorFromServer)

// special cases
blocker.events.add("Side menu closed", sideMenuClosed)
//blocker.events.add("CRM tab was changed", crmTabWasChanged)
blocker.events.add("tabSwitched", tabSwitched)

// user is logged into CRM, we need to get name of that user
function getUserName () {
    const session = document.cookie
    blocker.user.session = session
    blocker.socket.emit("getUserName", session)
    log("get User Name", session)
}
// user has logged just now and we need to set it's name and session
function setUserName (name) {
    if (!name) autorization.logout()
    const session = document.cookie
    const user = {
        name: name,
        session: session
    }
    blocker.socket.emit("setUserName", user)
    blocker.user = user
    log("setUserName", user)
}

function autorizationError (message) {
    reportError("autorizationError", message)
    alert("Не удалось авторизоваться на сервере блокировщика!\n"
    + "Пожалуйста, разлогиньтесь, обновите страницу (F5) и залогиньтесь снова.\n"
    + "Если это сообщение появляется уже в третий раз, позвоните Паше.")
}

function tableWasCreated () {
    log("tableWasCreated")
    blocker.table.created = true
    blocker.table.rows = rowsAmount()

    // special cases: crm tabs and side menu
    if (!tabsHandler.activated) {
        tabsHandler.activate()
        getExpandButton().addEventListener(
            "click", blocker.sideMenuObserver.start)
    } 
    
    // start chat
    if (!chat.created) chat.create(blocker)
    const rows = getRowsWithOrders()
    // for each row add listener
    rows.map(row => addEventToRow(row))
    // get list of blocked orders
    blocker.socket.emit("getOrdersList")
}
function addEventToRow (row) {
    if (row.attributes.blocker) return
    const sid = getSidFromRow(row)
    row.setAttribute("blocker", sid)
    row.addEventListener(
        "dblclick", () => {
            log("Click on row", [row, sid])
            blocker.events.emit("openOrder", sid)
        }
    )
}

function sideMenuClosed () {
    blocker.table.observer.start()
}

function tabSwitched() {
    log("tabSwitched")
    blocker.socket.emit("getOrdersList")
}
function tabIsClosing(orderWindowId) {
    log("tabIsClosing", orderWindowId)
    const orderWindow = blocker.windows.db.get.by.id(orderWindowId)
    if (!orderWindow) return
    blocker.events.emit("windowClosed", orderWindow)
}

function openOrder (sid) {
    log("openOrder", sid)
    // create the order window object
    const orderWindow = blocker.windows.create(sid)
    orderWindow.startObserver(
        windowDomObject => blocker.events.emit(
            "windowWasCreated", windowDomObject
        )
    )
}

function windowWasCreated(windowDomObject) {
    const windowId = windowDomObject.attributes.blocker.value
    log("windowWasCreated. windowId", windowId)
    log("windowWasCreated blocker.windows.db", blocker.windows.db)
    const orderWindow = blocker.windows.db.get.by.id(windowId)
    blocker.events.emit("addEventsToWindowButtons", orderWindow)
    // crm tabs have close button. We must handle it
    tabsHandler.addCloseButtonListener(orderWindow.id)
    if (blocker.orders.outdated.includes(orderWindow.order)) {
        // if order is outdated, emit corresponding event
        console.log("order is outdated")
        blocker.events.emit("orderIsBlocked", orderWindow.id)
        return
    }
    const data = {
        "window": orderWindow.id,
        "order" : orderWindow.order,
        "user"  : blocker.user,
        "tab"   : blocker.tab
    }
    blocker.socket.emit("try to block", data)
}

function orderIsBlocked (windowId) {
    const windowObject = blocker.windows.db.get.by.id(windowId)
    if (!windowObject) return
    log("Order is blocked++++++++", windowObject)
    windowObject.alreadyBlocked = true
    windowObject.removeSaveButton()
}

function addEventsToWindowButtons (windowObject) {
    // check if widnow still on screen or may be user closed it emidiately
    if (!windowObject.domElement) {
        blocker.events.emit("windowClosed", windowObject)
    }
    windowObject
        .addListenersToButtons(blocker.events)
}

function windowClosed (windowObject) {
    // this called only when close or cancel button is pressed
    log("Window close", windowObject)
    if (!windowObject.alreadyBlocked) blocker.socket.emit(
        "try to free order", windowObject.id)
    blocker.windows.destroy(windowObject.id)
}
function orderSaved (windowId) {
    // this called only when save button pressed
    log("orderSaved", windowId)
    blocker.socket.emit("orderSaved", windowId)
    blocker.windows.destroy(windowId)
}

function blockOrder (order) { // incoming
    log("*******BLOCK******", order)
    log("blockOrder", order.id)
    blocker.events.emit(
        "paintBlocked",
        {id:order.id, name:order.user.name}
    )
}
function freeOrder (sid) { // incoming
    log("****FREE****", sid)
    if (blocker.table.created) {
        blocker.events.emit("removeBlocks", sid)
    }
}
function orderChanged (data) {  // incoming
    log("****CHANGED****", data)
    blocker.orders.outdated.add(data.id)
    blocker.events.emit(
        "paintChanged",
        {id:data.id, name:data.user.name}
    )
}

function paintBlocked (data) {
    getRowsBySid(data.id)
        .forEach(
            row => toggleRowStyle(
                row, "block", data.name
            )
        )
}
function paintChanged (data) {
    getRowsBySid(data.id)
        .forEach(
            row => toggleRowStyle(
                row, "changed", data.name
            )
        )
}
function removeBlocks (sid) {
    getRowsBySid(sid)
        .forEach(row => {
            if (!row.hasAttribute("blocker-changed")) {
                toggleRowStyle(row, "free")
            }
        })
}

function errorFromServer (error) {
    reportError("Error from server", error)
    alert(
        "Внимание!\n"
        + "Пришла ошибка с сервера!\n"
        + "Нажмите F12, выберите \"console\" и сделайте скриншот."
        + "Этот скриншот отправьте Паше, и опишите, что вы делали до "
        + "возникновения ошибки.\n"
        + "Для продолжения работы, обновите страницу."
    )
}
