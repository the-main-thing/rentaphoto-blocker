const uuidv4 = require("uuid/v4")
import io from "socket.io-client"

import {
    TableMutationObserver,
    SideMenuObserver,
    TabSwitchObserver
} from "./customObservers.js"
import {CustomEvent} from "./customEvents.js"
import {OrderWindow} from "./window.js"
import {log} from "./helpers"

class Blocker {
    constructor(server) {
        const self = this
        this.server = server
        this.socket = io(server)
        this.tab = uuidv4()
        this.user = {
            session: null,
            name: null
        }
        this.events = new CustomEvent()
        this.table = {
            rows: 0, // for determinig is there new tab opened
            created: false,
            observer: new TableMutationObserver(self.events),
        }
        this.sideMenuObserver = new SideMenuObserver(self.events)
        this.tabSwitchObserver = new TabSwitchObserver(self)
        this.outdatedOrders = []
        this.orders = {
            outdated: {
                list:[],
                includes: orderId => this.orders.outdated.list.includes(orderId),
                add: orderId => this.orders.outdated.list.push(orderId)
            }

        }
        this.windows = {
            db: {
                // Window.id:Window,
                get: {
                    by: {
                        id: id => {
                            const result = self.windows.db[id]
                            log("blocker.db.get", result)
                            return result
                        },
                        // from database get all elements with given order ID
                        order: id => self.windows.db.filter(
                            windowObject => windowObject.order === id
                        )
                    }
                },
            },
            create: function (orderId) {
                const orderWindow = new OrderWindow(orderId, self.events)
                self.windows.db[orderWindow.id] = orderWindow
                return self.windows.db[orderWindow.id]
            },
            destroy: function (windowId) {
                delete self.windows.db[windowId]
            }
        }
    }
}

export { Blocker }
