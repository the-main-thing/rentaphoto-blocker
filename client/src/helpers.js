function reportError(text, object) {
    const d = new Date().toString()
    console.error("\n\nError\n\n", text, ":\n", d, "\n", object)
}

function log(text, object) {
    if (object === undefined) {
        object = ""
    }
    const decor = "___________________"
    const date = new Date()
    const t = date.toTimeString()
    console.log(
        `${decor}\n${t}:\n${text}\n${object}\n${decor}`
    )
}

function getSidFromRow(row) {
    const cells = row.getElementsByTagName("td")
    const firstCellExsists = Boolean(cells[0])
    if (firstCellExsists) {
        const firstCellText = cells[0].innerText.trim()
        if (firstCellText.match(/\d{2}-\d{6}/)) {
            return firstCellText
        }
    }
    return null
}

function getSidFromWindow(windowDomObject) {
    const sid = windowDomObject.querySelector(
        "div[id^=\"displayfield-\"][id$=\"-inputEl\"]"
    ).innerText.trim()
    log("getSidFromWindow", sid)
    return sid
}

function moreThanOneTab () {
    return document
        .querySelector(
            "div[id^=\"tabbar-\"][id$=\"-body\"]"
        ).innerText
        .split("Заявки ")
        .length > 2
}

function rowsAmount() {
    return document.querySelectorAll(
        "div>table[class=\"x-grid-table x-grid-table-resizer\"]>tbody>tr>td>table"
    ).length
}

export { reportError, log, getSidFromRow, moreThanOneTab, getSidFromWindow, rowsAmount }
