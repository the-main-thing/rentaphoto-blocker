import {reportError} from "./helpers.js"

function toggleRowStyle (row, action, name="") {
    const cells = row.querySelectorAll("td")
    switch (action) {
    case "block":
        row.addEventListener("mouseover", removeEditableCells)
        row.addEventListener("mouseleave", addEditableCells)
        cells.forEach(cell => {
            cell.style.backgroundColor = "rgb(255, 226, 236)"
            cell.title = name
        })
        break

    case "free":
        row.removeEventListener("mouseover", removeEditableCells)
        row.removeEventListener("mouseleave", addEditableCells)
        cells.forEach(cell => {
            cell.style.backgroundColor = ""
            cell.style.display = ""
        })
        break

    case "changed":
        cells.forEach(cell => {
            cell.style.backgroundColor = "#ff611c"
            cell.title = name
        })
        row.setAttribute("blocker-changed", "")
        break

    default:
        reportError("toggleStyle()", action)
        break
    }
}

function removeEditableCells () {
    // the "this" is expected to be the order row
    const cells = this.querySelectorAll("td")
    cells[1].style.display = "none"
    cells[4].style.display = "none"
    cells[12].style.display = "none"
    cells[0].colSpan = 2
    cells[3].colSpan = 2
    cells[11].colSpan = 2
}

function addEditableCells () {
    // the "this" is expected to be the order row
    const cells = this.querySelectorAll("td")
    cells[1].style.display = ""
    cells[4].style.display = ""
    cells[12].style.display = ""
    cells[0].colSpan = 1
    cells[3].colSpan = 1
    cells[11].colSpan = 1
}

export { toggleRowStyle }
