import {
    windowCreated,
    tableCreated,
    menuIsStilOpen,
    tabSwitch
} from "./mutationsCheckers.js"

import {
    rowsAmount
} from "./helpers"

import { getCrmTabsBar } from "./elementsGetters"

class TableMutationObserver {
    constructor(CustomEventsObject) {
        // !!!
        const self = this
        // !!!
        this.observer = new MutationObserver(function (mutations) {
            for (let i = 0, n = mutations.length; i < n; i++) {
                const mutation = mutations[i]
                if (tableCreated(mutation)) {
                    if (self.observing) {
                        self.observing = false
                        CustomEventsObject.emit("tableWasCreated")
                        self.stop()
                        return
                    }
                }
            }
        })
        this.observing = false
        this.start = function () {
            self.observing = true
            self.observer.observe(
                document.querySelector("body"),
                {
                    attributes: false,
                    childList: true,
                    subtree: true }
            )
            CustomEventsObject.emit("tableObserverStarted")
        }
        this.stop = function () {
            self.observer.disconnect()
            self.observing = false
            CustomEventsObject.emit("tableObserverStoped")
        }
        this.restart = function () {
            if (self.observing) {
                self.stop()
            }
            self.start()
            CustomEventsObject.emit("tableObserverRestarted")
        }
    }
}

class WindowMutationObserver {
    constructor(orderId, callback) {
        this.order = orderId
        this.callback = callback
        this.observing = false
        // !!!
        const self = this
        this.observer = new MutationObserver(mutations => {
            mutations.forEach(mutation => {
                if (!self.observing) return
                const createdWindow = windowCreated(mutation, self.order)
                if (createdWindow) {
                    console.log("Window FIRE!!!!")
                    self.stop()
                    self.callback(createdWindow)
                }
            })
        })
    }
    start () {
        this.observing = true
        this.observer.observe(
            document.querySelector("body"),
            {
                attributes: false,
                childList: true,
                subtree: true }
        )
    }
    stop () {
        this.observing = false
        this.observer.disconnect()
    }
}

class SideMenuObserver {
    constructor(CustomEventsObject) {
        const self = this
        this.observer = new MutationObserver((mutations, observer) => {
            for (let i = 0, n = mutations.length; i < n; i++) {
                const mutation = mutations[i]
                if (mutation.type === "childList") {
                    if (!menuIsStilOpen()) {
                        CustomEventsObject.emit("Side menu closed")
                        return observer.disconnect()
                    }
                }
            }
        }),
        this.start = () => {
            console.log("side menu observer start")
            self.observer.observe(
                document.querySelector("body"),
                {
                    attributes: false,
                    childList: true,
                    subtree: true }
            )
        }
    }
}

class TabsBarObserver {
    start(callback, tabBarElement) {
        const observer = new MutationObserver((mutations, observer) => {
            let observing = true
            for (const mutation of mutations) {
                if (mutation.type === "childList" && observing) {
                    if (mutation.addedNodes.length > 0 && observing) {
                        observing = false
                        console.log("tab added!")
                        callback()
                        observer.disconnect()
                    }
                }
            }
        })
        observer.observe(
            tabBarElement,
            {
                attributes: false,
                childList: true,
                subtree: true
            }
        )
    }
}

class TabSwitchObserver {
    constructor(tabElement) {
        this.element = tabElement
    }
    start(callback) {
        const observer = new MutationObserver((mutations, observer) => {
            let observing = true
            for (const mutation of mutations) {
                if (mutation.type === "attributes" && observing) {
                    if (mutation.attributeName === "class" && observing) {
                        observing = false
                        console.log("Tab switched!")
                        callback()
                        observer.disconnect()
                    }
                }
            }
        })
        observer.observe(
            this.element,
            {
                attributes: true,
                childList: false,
                subtree: false
            }
        )
    }
}

export {
    TableMutationObserver,
    WindowMutationObserver,
    SideMenuObserver,
    TabSwitchObserver,
    TabsBarObserver
}
